<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\ActiveDataProvider;
use app\models\Puerto;
use app\models\Etapa;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
          
                // Consulta 1 mediante Active Record
    $dataProvider = new ActiveDataProvider([
        'query'=> Puerto::find()
            ->select(["dorsal","nompuerto"])
            ->distinct()
            ->where(['<', 'altura', 1000]),
    ]);

    // Consulta 2 mediante Active Record
    $dataProvider2 = new ActiveDataProvider([
        'query'=> Etapa::find()
            ->select(["numetapa","kms"]) 
            ->where(['<>', 'salida', 'llegada']), 
    ]);
     // Consulta 3 mediante Active Record
    $dataProvider3 = new ActiveDataProvider([
        'query'=> Puerto::find()
            ->select(["nompuerto","dorsal"]) 
            ->where(['<', 'altura', 1500]), 
    ]);

    return $this->render("index",[
        "resultados"=>$dataProvider,
        "resultados2"=>$dataProvider2, 
        "resultados3"=>$dataProvider3, 
        "campos"=>['dorsal','nompuerto'],
        "campos2"=>['numetapa','kms'], 
        "campos3"=>['nompuerto','dorsal'], 
        "titulo"=>"Consulta 1 con Active Record",
        "titulo2"=>"Consulta 2 con Active Record", 
        "titulo3"=>"Consulta 3 con Active Record",
        "enunciado"=>"Los puertos con altura menor a 1000 metros figurando el nombre del puerto y el dorsal del ciclista que lo ganó.",
        "enunciado2"=>"Las etapas no circulares mostrando sólo el número de etapa y la longitud de las mismas",
        "enunciado3"=>"Los puertos con altura mayor a 1500 metros figurando el nombre del puerto y el dorsal del ciclista que los ganó",
        "sql"=>"SELECT DISTINCT nompuerto, dorsal FROM puerto WHERE altura<1000",
        "sql2"=>"SELECT DISTINCT numetapa,kms FROM etapa WHERE salida<>llegada", 
        "sql3"=>"SELECT DISTINCT numpuerto, dorsal FROM puerto WHERE altura>1500", 
    ]);   
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
//       public function action(){
//          //mediante Active Record 
//        $dataProvider = new ActiveDataProvider([
//            'query'=> Ciclista::find()->select("nompuerto, dorsal")->distinct()->where(['<', 'altura', 1000]),
//            
//        ]);
//        
//                return $this->render("site/index",[
//                    "resultados"=>$dataProvider,
//                    "campos"=>['nompuerto,dorsal'],
//                    "titulo"=>"Consulta 1 con Active Record",
//                    "enunciado"=>"Los puertos con altura menor a 1000 metros figurando el nombre del puerto y el dorsal del ciclista que lo ganó.",
//                    "sql"=>"SELECT DISTINCT nompuerto, dorsal FROM puerto WHERE altura<1000",
//                    
//                ]);
//             
//    } 
    
}
