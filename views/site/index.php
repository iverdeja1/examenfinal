<?php
use yii\grid\GridView;
?>

<div class="jumbotron">
    <h2><?=$titulo?></h2>
    
    <p class="lead"><?= $enunciado ?></p>
    <p class="well">
    <?=$sql ?>
    </div>
</div>

<?= GridView::widget([
    'dataProvider' => $resultados,
    'columns'=>$campos
]); ?>

 
<div class="jumbotron">
    <h2><?=$titulo2?></h2>
    
    <p class="lead"><?= $enunciado2 ?></p>
    <p class="well">
    <?=$sql2 ?>
    </div>
</div>

<?= GridView::widget([
    'dataProvider' => $resultados2,
    'columns'=>$campos2
]); ?>


<div class="jumbotron">
    <h2><?=$titulo3?></h2>
    
    <p class="lead"><?= $enunciado3 ?></p>
    <p class="well">
    <?=$sql3 ?>
    </div>
</div>

<?= GridView::widget([
    'dataProvider' => $resultados3,
    'columns'=>$campos3
]); ?>




